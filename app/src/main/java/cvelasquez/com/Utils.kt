package cvelasquez.com

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

fun Activity.toastShort(menssages: String) {
    Toast.makeText(this, menssages, Toast.LENGTH_SHORT).show()
}

fun Activity.toastLong(menssages: String) {
    Toast.makeText(this, menssages, Toast.LENGTH_LONG).show()
}

fun ViewGroup.inflate(layoutID: Int):View{
    return LayoutInflater.from(context).inflate(layoutID, this, false)
}
